# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

long_desc = '''
This package contains the Cerbere extension for various radiometer datasets.
'''

requires = [
   # 'cerbere>=2.0.0',
]

setup(
    name='cerberecontrib-radiometer',
    version='1.0',
    url='',
    license='GPLv3',
    authors='Jean-François Piollé',
    author_email='jfpiolle@ifremer.fr',
    description='Cerbere extension for radiometer products',
    long_description=long_desc,
    zip_safe=False,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    entry_points={
        'cerbere.plugins': [
            'SMOSWindNCDataset = cerberecontrib_radiometer.dataset.smoswindncdataset:SMOSWindNCDataset',
            'MAXSSSMAPAscNCDataset = cerberecontrib_radiometer.dataset.maxssncdataset:MAXSSSMAPAscNCDataset',
            'MAXSSSMAPDescNCDataset = cerberecontrib_radiometer.dataset.maxssncdataset:MAXSSSMAPDescNCDataset',
            'MAXSSWindsatAscNCDataset = cerberecontrib_radiometer.dataset.maxssncdataset:MAXSSWindsatAscNCDataset',
            'MAXSSWindsatDescNCDataset = cerberecontrib_radiometer.dataset.maxssncdataset:MAXSSWindsatDescNCDataset',
            'MAXSSAMSR2AscNCDataset = cerberecontrib_radiometer.dataset.maxssncdataset:MAXSSAMSR2AscNCDataset',
            'MAXSSAMSR2DescNCDataset = cerberecontrib_radiometer.dataset.maxssncdataset:MAXSSAMSR2DescNCDataset',
            'TRMM3B42HDFDataset = '
            'cerberecontrib_radiometer.dataset.trmm3b42hdfdataset'
            ':TRMM3B42HDFDataset',
            'NSIDC0080Dataset = cerberecontrib_radiometer.dataset.nsidc0080dataset:NSIDC0080Dataset',
        ]
    },
    platforms='any',
    packages=find_packages(),
    include_package_data=True,
    install_requires=requires,
)
