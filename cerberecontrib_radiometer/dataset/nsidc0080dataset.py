"""
Dataset class for NSIDC 0080 product : Near-Real-Time DMSP SSM/I-SSMIS Daily
Polar Gridded Brightness Temperature
"""
import datetime
from pathlib import Path

import numpy as np
import xarray as xr

from cerbere.dataset.dataset import Dataset
import cerform.grid.nsidcgrid as nsidc


class NSIDC0080Dataset(Dataset):
    """Dataset class for NSIDC 0080 product files
    """
    def _open_dataset(self, **kwargs):
        if self.url is None:
            raise NotImplementedError

        with open(self.url, 'rb') as fid:
            data = np.fromfile(fid, np.int16)
        data = np.ma.masked_values(data, 0)

        # reshape data
        if data.size == 448 * 304:
            pole = nsidc.NORTH
            resolution = nsidc.RES25
            data = data.reshape((448, 304)) / 10.
        elif data.size == 896 * 608:
            pole = nsidc.NORTH
            resolution = nsidc.RES12
            data = data.reshape((896, 608)) / 10.
        elif data.size == 332 * 316:
            pole = nsidc.SOUTH
            resolution = nsidc.RES25
            data = data.reshape((332, 316)) / 10.
        elif data.size == 664 * 632:
            pole = nsidc.SOUTH
            resolution = nsidc.RES12
            data = data.reshape((664, 632)) / 10.
        else:
            raise ValueError("Unknown file size")

        attrs = Path(self.url).name.split('_')
        date = datetime.datetime.strptime(attrs[2], '%Y%m%d') \
            + datetime.timedelta(hours=12)

        vlat, vlon = nsidc.get_latlon(pole, resolution)
        lon = xr.DataArray(data=vlon, dims=['y', 'x'])
        lat = xr.DataArray(data=vlat, dims=['y', 'x'])
        time = xr.DataArray(data=[date], dims=['time'])
        var = xr.DataArray(data=data, dims=['y', 'x'])

        var.attrs['myvar'] = 'test_attr_val'
        dst = xr.Dataset(
            coords={'lat': lat, 'lon': lon, 'time': time},
            data_vars={'brightness_temperature': var}
        )
        dst.attrs['platform'] = attrs[1].upper()
        dst.attrs['instrument'] = "SSM/I"
        dst.attrs['band'] = "{} GHz".format(attrs[4][1:3])
        dst.attrs['polarization'] = attrs[4][3]
        dst.attrs['time_coverage_start'] = datetime.datetime.strptime(
            attrs[2], '%Y%m%d')
        dst.attrs['time_coverage_end'] = datetime.datetime.strptime(
            attrs[2], '%Y%m%d') + datetime.timedelta(hours=24)
        dst.attrs['pole'] = pole
        dst.attrs['resolution'] = resolution

        return dst
