# -*- coding: utf-8 -*-
"""
:mod:`~cerbere.dataset` class for SMOSWind L2 netcdf files
"""
import datetime
from pathlib import Path

import numpy as np
import xarray as xr

from cerbere.dataset.dataset import Dataset


class TRMM3B42HDFDataset(Dataset):
    """Dataset class for SMOSWind netcdf files
    """
    @staticmethod
    def _reformat(dst, source=None):
        is_timeseries = False
        if source is None:
            is_timeseries = True
            source = dst.encoding['source']

        # grid center time
        ctime = np.datetime64(datetime.datetime.strptime(
            Path(source).name, '3B42.%Y%m%d.%H.7.HDF').isoformat())

        # reformat to CF rules
        if is_timeseries:
            # time series of grids
            dst['time'] = xr.DataArray(data=[ctime], dims=('time',))
            dst = dst.rename(
                {'nlon': 'lon',
                 'nlat': 'lat',
                 'satObservationTime': 'measurement_time'})
            dst['measurement_time'].attrs['long_name'] = \
                'measurement time, as a difference in minutes'
            dst['measurement_time'].encoding['units'] = 'minute'
        else:
            dst = dst.rename(
                {'nlon': 'lon', 'nlat': 'lat', 'satObservationTime': 'time'})
            dst['time'] = dst.time + ctime

        resolution = 0.25
        dst['lon'] = xr.DataArray(
            data=np.arange(-180 + resolution / 2., 180, 0.25),
            attrs={'long_name': 'longitude'},
            dims=['lon']
        )
        dst['lat'] = xr.DataArray(
            data=np.arange(-50 + resolution / 2., 50, 0.25),
            attrs={'long_name': 'latitude'},
            dims=['lat']
        )

        # drop dummy variables
        dst = dst.drop([
            'InputFileNames', 'InputAlgorithmVersions',
            'InputGenerationDateTimes'])

        # _FillValues
        for v in dst.data_vars:
            if v == 'time':
                continue
            dst.data_vars[v].encoding['_FillValue'] = 9999.9

        # decompose attributes
        for header in ['FileHeader', 'FileInfo', 'GridHeader']:
            attrs = dst.attrs.pop(header).strip()
            for attr in attrs.split(';\n'):
                k, v = attr.split('=')
                dst.attrs[k] = v

        return dst

    def _open_dataset(self, **kwargs):
        if isinstance(self._url, list):
            kwargs['preprocess'] = self._reformat
            return super(TRMM3B42HDFDataset, self)._open_dataset(**kwargs)

        return self._reformat(
            super(TRMM3B42HDFDataset, self)._open_dataset(**kwargs),
            source=Path(self._url).name
        )


