# -*- coding: utf-8 -*-
"""
:mod:`~cerbere.dataset` class for MAXSS Radiometer netcdf files
"""
import datetime
from pathlib import Path

import numpy as np
import xarray as xr

from cerbere.dataset.ncdataset import NCDataset


class MAXSSSMOSNCDataset(NCDataset):
    """Dataset class for SMOSWind netcdf files
    """
    def _open_dataset(self, **kwargs):
        # use pixel time as true time field
        dst = super(MAXSSSMOSNCDataset, self)._open_dataset(**kwargs)
        dst = dst.drop('time').squeeze()
        dst = dst.rename({'measurement_time': 'time'})

        return dst


class MAXSS1DNCDataset(NCDataset):
    """Dataset class for MAXSS daily radiometer files netcdf files
    """
    def _open_dataset(self, node='A', **kwargs):
        # use pixel time as true time field
        dst = super(MAXSS1DNCDataset, self)._open_dataset(**kwargs)

        if 'node' in dst.dims:
            dst = dst.rename({'node': 'pass'})

        if node == 'A':
            dst = dst.isel({'pass':0})
        else:
            dst = dst.isel({'pass':1})

        values = dst['lon'].values
        values[values>180] = values[values>180] - 360.
        dst['lon'] = values

        # center on Greenwich
        dst = dst.roll({'lon':len(values) // 2}, roll_coords=True)

        return dst


class MAXSSSMAPNCDataset(MAXSS1DNCDataset):
    """Dataset class for MAXSS SMAP daily radiometer files netcdf files
    """
    def _open_dataset(self, **kwargs):
        dst = super(MAXSSSMAPNCDataset, self)._open_dataset(**kwargs)

        minutes = dst['minute'].values
        wind = dst['wind'].values
        minutes = np.ma.masked_where(wind==0., minutes)
        dst['wind'][:] = np.ma.masked_where(wind==0., wind)

        # add time
        dst['time'] = xr.DataArray(
            data=np.datetime64(
                f'{dst.attrs["year_of_observation"]}-'
                f'{dst.attrs["month_of_observation"]:02d}-'
                f'{dst.attrs["day_of_month_of observation"]:02d}') + minutes,
            dims=('lat', 'lon'))
        dst  = dst.drop(['minute'])

        return dst


class MAXSSSMAPAscNCDataset(MAXSSSMAPNCDataset):
    """Dataset class for MAXSS SMAP daily radiometer files netcdf files
    """
    def _open_dataset(self, **kwargs):
        return super(MAXSSSMAPAscNCDataset, self)._open_dataset(node='A', **kwargs)


class MAXSSSMAPDescNCDataset(MAXSSSMAPNCDataset):
    """Dataset class for MAXSS SMAP daily radiometer files netcdf files
    """
    def _open_dataset(self, **kwargs):
        return super(MAXSSSMAPDescNCDataset, self)._open_dataset(node='D', **kwargs)


class MAXSSWindsatNCDataset(MAXSS1DNCDataset):
    """Dataset class for MAXSS Windsat daily radiometer files netcdf files
    """
    def _open_dataset(self, **kwargs):
        dst = super(MAXSSWindsatNCDataset, self)._open_dataset(
            decode_times=False, **kwargs)

        minutes = dst['mingmt'].values
        nodata = dst['nodata'].values
        minutes = np.ma.masked_where(nodata, minutes)

        reftime = datetime.datetime.strptime(Path(self._url).name.lstrip(
            'wsat_')[:8], '%Y%m%d')
        dst['time'] = xr.DataArray(
            data=np.datetime64(reftime) + minutes.astype('timedelta64[m]'),
            dims=('lat', 'lon'))
        dst  = dst.drop(['mingmt'])

        return dst


class MAXSSWindsatAscNCDataset(MAXSSWindsatNCDataset):
    """Dataset class for MAXSS Windsat daily radiometer files netcdf files
    """
    def _open_dataset(self, **kwargs):
        return super(MAXSSWindsatAscNCDataset, self)._open_dataset(node='A', **kwargs)


class MAXSSWindsatDescNCDataset(MAXSSWindsatNCDataset):
    """Dataset class for MAXSS Windsat daily radiometer files netcdf files
    """
    def _open_dataset(self, **kwargs):
        return super(MAXSSWindsatAscNCDataset, self)._open_dataset(node='D', **kwargs)


class MAXSSAMSR2NCDataset(MAXSS1DNCDataset):
    """Dataset class for MAXSS AMSR-2 daily radiometer files netcdf files
    """
    def _open_dataset(self, **kwargs):
        dst = super(MAXSSAMSR2NCDataset, self)._open_dataset(**kwargs)

		# remove valid min/max
        dst['lon'].attrs.pop('valid_min')
        dst['lon'].attrs.pop('valid_max')
        dst['time'].attrs.pop('valid_min')
        dst['time'].attrs.pop('valid_max')

        return dst


class MAXSSAMSR2AscNCDataset(MAXSSAMSR2NCDataset):
    """Dataset class for MAXSS AMSR-2 daily radiometer files netcdf files
    """
    def _open_dataset(self, **kwargs):
        return super(MAXSSAMSR2AscNCDataset, self)._open_dataset(node='A', **kwargs)


class MAXSSAMSR2DescNCDataset(MAXSSAMSR2NCDataset):
    """Dataset class for MAXSS AMSR-2 daily radiometer files netcdf files
    """
    def _open_dataset(self, **kwargs):
        return super(MAXSSAMSR2DescNCDataset, self)._open_dataset(node='D', **kwargs)


