# -*- coding: utf-8 -*-
"""
:mod:`~cerbere.dataset` class for SMOSWind L2 netcdf files
"""
from cerbere.dataset.ncdataset import NCDataset


class SMOSWindNCDataset(NCDataset):
    """Dataset class for SMOSWind netcdf files
    """
    def _open_dataset(self, **kwargs):
        # use pixel time as true time field
        dst = super(SMOSWindNCDataset, self)._open_dataset(**kwargs)
        dst = dst.drop('time').squeeze()
        dst = dst.rename({'measurement_time': 'time'})
        print(dst)

        return dst