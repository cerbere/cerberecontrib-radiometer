"""

Test class for cerbere SMOSWind L2 dataset

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import unittest

from tests.checker import Checker


class SMOSWindNCDatasetChecker(Checker, unittest.TestCase):
    """Test class for GRIB files"""

    def __init__(self, methodName="runTest"):
        super(SMOSWindNCDatasetChecker, self).__init__(methodName)

    @classmethod
    def dataset(cls):
        """Return the mapper class name"""
        return 'SMOSWindNCDataset'

    @classmethod
    def feature(cls):
        """Return the related feature class name"""
        return 'CylindricalGrid'

    @classmethod
    def test_file(cls):
        """Return the name of the test file for this test"""
        return "SM_OPER_MIR_SCNFSW_20200628T155751_20200628T173954_110_001_7.nc"

    @classmethod
    def download_url(cls):
        """Return the URL of the data test repository where to get the test
        files
        """
        return "ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/"
