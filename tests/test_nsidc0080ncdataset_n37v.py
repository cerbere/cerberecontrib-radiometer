"""

Test class for cerbere NSIDC 0080 dataset

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import unittest

from tests.checker import Checker


class NSIDC0080DatasetChecker(Checker, unittest.TestCase):
    """Test class"""

    def __init__(self, methodName="runTest"):
        super(NSIDC0080DatasetChecker, self).__init__(methodName)

    @classmethod
    def dataset(cls):
        """Return the mapper class name"""
        return 'NSIDC0080Dataset'

    @classmethod
    def feature(cls):
        """Return the related feature class name"""
        return 'Grid'

    @classmethod
    def test_file(cls):
        """Return the name of the test file for this test"""
        return "tb_f18_20210107_nrt_n37v.bin"

    @classmethod
    def download_url(cls):
        """Return the URL of the data test repository where to get the test
        files
        """
        return "ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/"
