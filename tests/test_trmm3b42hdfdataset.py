"""

Test class for cerbere TRMM 3B42 dataset

:copyright: Copyright 2021 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import unittest

from tests.checker import Checker


class TRMM3B42HDFDatasetChecker(Checker, unittest.TestCase):
    """Test class for GRIB files"""

    def __init__(self, methodName="runTest"):
        super(TRMM3B42HDFDatasetChecker, self).__init__(methodName)

    @classmethod
    def dataset(cls):
        """Return the mapper class name"""
        return 'TRMM3B42HDFDataset'

    @classmethod
    def feature(cls):
        """Return the related feature class name"""
        return 'CylindricalGrid'

    @classmethod
    def test_file(cls):
        """Return the name of the test file for this test"""
        return "3B42.20180101.00.7.HDF"

    @classmethod
    def download_url(cls):
        """Return the URL of the data test repository where to get the test
        files
        """
        return "ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/"
