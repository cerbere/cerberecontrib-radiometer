=========================
cerberecontrib-radiometer
=========================

Cerbere Dataset classes for various passive microwave radiometer products,
including:

  * NSIDC0080Dataset : NSIDC Near-Real-Time DMSP SSM/I-SSMIS Daily Polar Gridded
    Brightness Temperature product (0080)
  * SMOSWindNCDataset : L2 products from ESA/SMOSWind project
  * TRMM3B42HDFDataset : TRMM 3B42 L4 precipitation products from NASA GSFC


.. code-block:: python

  # example : opening a NSIDC product file
  import cerbere

  dst = cerbere.open_dataset(
      'tb_f18_20210107_nrt_s37v.bin',
      'NSIDC0080Dataset',
  )

